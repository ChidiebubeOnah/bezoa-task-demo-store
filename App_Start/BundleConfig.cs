﻿using System.Web;
using System.Web.Optimization;

namespace Store_Management_App
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Content/Scripts/jquery.validate*"));


            bundles.Add(new ScriptBundle("~/bundles/js")
                .Include("~/Content/assets/js/vendor/modernizr-3.6.0.min.js",
                    "~/Content/assets/js/vendor/jquery-3.5.1.min.js",
                    "~/Content/assets/js/vendor/jquery-migrate-3.3.0.min.js",
                    "~/Content/assets/js/vendor/bootstrap.bundle.min.js",
                    "~/Content/assets/js/plugins/slick.js",
                    "~/Content/assets/js/plugins/jquery.syotimer.min.js",
                    "~/Content/assets/js/plugins/jquery.nice-select.min.js",
                    "~/Content/assets/js/plugins/wow.js",
                    "~/Content/assets/js/plugins/jquery-ui-touch-punch.js",
                    "~/Content/assets/js/plugins/jquery-ui.js",
                    "~/Content/assets/js/plugins/magnific-popup.js",
                    "~/Content/assets/js/plugins/sticky-sidebar.js",
                    "~/Content/assets/js/plugins/easyzoom.js",
                    "~/Content/assets/js/plugins/scrollup.js",
                    "~/Content/Scripts/bootbox.js",
                    "~/Content/assets/js/main.js"));


            bundles.Add(new ScriptBundle("~/Scripts/datatables")
                .Include("~/Content/Admin/vendors/datatables.net/js/jquery.dataTables.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-dt/js/dataTables.dataTables.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-buttons/js/buttons.flash.min.js")
                .Include("~/Content/Admin/vendors/jszip/dist/jszip.min.js")
                .Include("~/Content/Admin/vendors/pdfmake/build/pdfmake.min.js")
                .Include("~/Content/Admin/vendors/pdfmake/build/vfs_fonts.js")
                .Include("~/Content/Admin/vendors/datatables.net-buttons/js/buttons.html5.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-buttons/js/buttons.print.min.js")
                .Include("~/Content/Admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js")
                .Include("~/Content/Admin/dist/js/dataTables-data.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/vendors/jquery").Include(
                "~/Content/Admin/vendors/jquery/dist/jquery.min.js")
                .Include("~/Content/Admin/vendors/popper.js/dist/umd/popper.min.js")
                .Include("~/Content/Admin/vendors/bootstrap/dist/js/bootstrap.min.js")
                .Include("~/Content/Admin/vendors/jquery-toggles/toggles.min.js")
                .Include("~/Content/Admin/vendors/moment/min/moment.min.js")
                .Include("~/Content/Admin/vendors/daterangepicker/daterangepicker.js")
                .Include("~/Content/Admin/dist/js/daterangepicker-data.js")
            );

            bundles.Add(new ScriptBundle("~/bundles/dist/jquery")
                .Include("~/Content/Admin/dist/js/jquery.slimscroll.js")
                .Include("~/Content/Admin/dist/js/dropdown-bootstrap-extended.js")
                .Include("~/Content/Admin/dist/js/feather.min.js")
                .Include("~/Content/Admin/dist/js/toggle-data.js")
                .Include("~/Content/Admin/dist/js/tooltip-data.js")
                .Include("~/Content/Scripts/bootbox.js")
                .Include("~/Content/Admin/dist/js/jquery.slimscroll.js")
                .Include("~/Content/Admin/dist/js/init.js")


            );

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Content/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/Scripts/bootstrap.js"));

           
            bundles.Add(new StyleBundle("~/Content/admincss")
                .Include("~/Content/Admin/vendors/datatables.net-dt/css/jquery.dataTables.min.css")
                .Include("~/Content/Admin/vendors/datatables.net-responsive-dt/css/responsive.dataTables.min.css")
                .Include("~/Content/Admin/vendors/daterangepicker/daterangepicker.css")
                .Include("~/Content/Admin/vendors/jquery-toggles/css/toggles.css")
                .Include("~/Content/Admin/vendors/jquery-toggles/css/themes/toggles-light.css")
                .Include("~/Content/Admin/dist/css/style.css")
            );


            bundles.Add(new StyleBundle("~/Content/assets")
                .Include("~/Content/assets/css/vendor/bootstrap.min.css")
                .Include("~/Content/assets/css/vendor/signericafat.css")
                .Include("~/Content/assets/css/vendor/cerebrisans.css")
                .Include("~/Content/assets/css/vendor/simple-line-icons.css")
                .Include("~/Content/assets/css/vendor/elegant.css")
                .Include("~/Content/assets/css/vendor/linear-icon.css")
                .Include("~/Content/assets/css/plugins/nice-select.css")
                .Include("~/Content/assets/css/plugins/easyzoom.css")
                .Include("~/Content/assets/css/plugins/slick.css")
                .Include("~/Content/assets/css/plugins/animate.css")
                .Include("~/Content/assets/css/plugins/magnific-popup.css")
                .Include("~/Content/assets/css/style.css")
            );
        }
    }
}
