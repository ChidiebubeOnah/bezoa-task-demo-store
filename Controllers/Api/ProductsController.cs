﻿using System.Web.Http;
using Store_Management_App.Interfaces;
using Store_Management_App.Repositories;

namespace Store_Management_App.Controllers.Api
{
   
    [Authorize(Roles = "Admin")]
    public class ProductsController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;


        public ProductsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }



        [HttpGet]
        public IHttpActionResult GetAllProducts()
        {
            return Ok(_unitOfWork.ProductRepository.GetAllProducts());

        }
       

        [HttpGet]
        public IHttpActionResult GetProduct(int id)
        {
            var product = _unitOfWork.ProductRepository.GetProductDataView(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }


        [HttpDelete]
        public IHttpActionResult DeleteProduct(int id)
        {
            if (_unitOfWork.ProductRepository.DeleteProduct(id))
            {
                return Ok();
            }

            return NotFound();

        }
    }
}