﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DashBoardController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;


        public DashBoardController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        public ActionResult Activities()
        {
            return View(_unitOfWork.ActivityRepository.GetActivities());
        }


        public ActionResult AddCategory()
        {
            var model = new CategoryViewModel
            {
                Categories = _unitOfWork.CategoryRepository.GetAllCategories()
            };
            return View(model);
        }


        public ActionResult AddVendor()
        {
            var model = new VendorViewModel
            {
                Vendors = _unitOfWork.VendorRepository.GetAllVendors()
            };
            return View(model);
        }

        public ActionResult EditCategory(int id)
        {
            var cate = _unitOfWork.CategoryRepository.GetCategory(id);
            if (cate == null) return HttpNotFound();

            var model = new CategoryViewModel
            {
                Id = cate.Id,
                Name = cate.Name,
                Categories = _unitOfWork.CategoryRepository.GetAllCategories()
            };
            return View("AddCategory", model);
        }


        public ActionResult EditVendor(int id)
        {
            var vend = _unitOfWork.VendorRepository.GetVendor(id);
            if (vend == null) return HttpNotFound();

            var model = new VendorViewModel
            {
                Id = vend.Id,
                Name = vend.Name,
                Vendors = _unitOfWork.VendorRepository.GetAllVendors()
            };
            return View("AddVendor", model);
        }


        public ActionResult Index()
        {
            var model = new DashboardViewModel
            {
                Customers = _unitOfWork.AppUserRepository.GetAllUsers("Customer").Count(),
                Orders = _unitOfWork.OrderRepository.GetAllOrders().Count(),
                Products = _unitOfWork.ProductRepository.GetAllProducts().Count(),
                TotalSales = _unitOfWork.OrderRepository.TotalSales(),
                Activities = _unitOfWork.ActivityRepository.GetActivities().Take(4)
            };
            return View(model);
        }

        public ActionResult Order(int id)
        {
            var order = _unitOfWork.OrderRepository.GetOrder(id);
            if (order == null) return HttpNotFound();

            return View(order);
        }

        public ActionResult Orders()
        {
            return View();
        }

      

        public ActionResult Products()
        {
            return View();
        }

        public ActionResult Users()
        {
            return View();
        }

       

      

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCategory(CategoryViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            if (model.Id == 0)
            {
                var category = new Category
                {
                    Name = model.Name,
                    UpdatedAt = model.UpdatedAt,
                    CreatedAt = model.CreatedAt
                };

                _unitOfWork.CategoryRepository.CreateCategory(category);
                TempData["success"] = "Category Successfully Added!";
                return RedirectToAction("AddCategory", "DashBoard");
            }

            var cate = _unitOfWork.CategoryRepository.GetCategory(model.Id);
            if (cate != null)
                if (_unitOfWork.CategoryRepository.UpdateCategory(model.Id, model))
                {
                    TempData["success"] = "Category Successfully Updated!";
                    return RedirectToAction("AddCategory", "DashBoard");
                }

            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVendor(VendorViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            if (model.Id == 0)
            {
                var vendor = new Vendor
                {
                    Name = model.Name,
                    UpdatedAt = model.UpdatedAt,
                    CreatedAt = model.CreatedAt
                };

                _unitOfWork.VendorRepository.CreateVendor(vendor);
                TempData["success"] = "Vendor Successfully Added!";
                return RedirectToAction("AddVendor", "DashBoard");
            }

            var vend = _unitOfWork.VendorRepository.GetVendor(model.Id);
            if (vend != null)
                if (_unitOfWork.VendorRepository.UpdateVendor(model.Id, model))
                {
                    TempData["success"] = "Vendor Successfully Updated!";
                    return RedirectToAction("AddVendor", "DashBoard");
                }

            return View(model);
        }

        public ActionResult AddProduct()
        {
            var model = new ProductViewModel
            {
                Categories = _unitOfWork.CategoryRepository.GetAllCategories(),
                Vendors = _unitOfWork.VendorRepository.GetAllVendors()
            };

            return View(model);
        }

        public ActionResult EditProduct(int id)
        {
            var productInDb = _unitOfWork.ProductRepository.GetProduct(id);
            if (productInDb == null) return HttpNotFound();
            var model = Mapper.Map<Product, ProductViewModel>(productInDb);
            model.Categories = _unitOfWork.CategoryRepository.GetAllCategories();
            model.Vendors = _unitOfWork.VendorRepository.GetAllVendors();


            return View("AddProduct", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(ProductViewModel model, HttpPostedFileBase photo = null)
        {
            if (!ModelState.IsValid)
            {
                model.Categories = _unitOfWork.CategoryRepository.GetAllCategories();


                return View("AddProduct", model);
            }

            if (_unitOfWork.ProductRepository.SaveProduct(model, photo) == "new")
                TempData["prodMsg"] = "Product Successfully Added";
            else
                TempData["prodMsg"] = $"{model.Name} Successfully Updated";

            return RedirectToAction("Products");
        }


        [AllowAnonymous]
        public FileContentResult GetMedia(int id)
        {
            var prod = _unitOfWork.ProductRepository.GetProduct(id);
            if (prod != null) return File(prod.File.Content, prod.File.ContentType);
            return null;
        }
    }
}