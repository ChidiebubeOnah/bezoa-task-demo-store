﻿using System;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Controllers
{
    public class ShopController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        private const int PageSize = 4;

        public ShopController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Shop
        public ActionResult Index(string category, string vendor, int page = 1, string query = null)
        {
            var model = new ProductPageViewModel
            {
                Products = _unitOfWork.ProductRepository.ProductsPerPage(PageSize, category, vendor, query, page),
                Pagination = new Pagination
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = _unitOfWork.ProductRepository.GetAllProducts(category, vendor).Count()
                },
                SelectedCategory = category,
                SelectedVendor = vendor,
                CartFormViewModel = new CartFormViewModel
                {
                    Quantity = 1
                }
            };
            if (!string.IsNullOrWhiteSpace(query))
            {
                ViewBag.Search = true;
                model.Pagination.TotalItems = _unitOfWork.ProductRepository.GetAllProductsPerSearch(query).Count();
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(string search)
        {
            return RedirectToAction("Index", "Shop", new {query = search});
        }


        public ActionResult Details(int id)
        {
            var model = new ProdDetailViewModel
            {
                Product = _unitOfWork.ProductRepository.GetProductDataView(id),
                CartFormViewModel = new CartFormViewModel
                {
                    ProductId = id,
                    Quantity = 1
                }
            };


            if (model.Product != null) return PartialView(model);

            return HttpNotFound();
        }

        public PartialViewResult SideBarCategories(string category)
        {
            ViewBag.SelectedCategory = category;

            var categories = _unitOfWork.ProductRepository.GetAllProducts()
                .Select(p => p.Category).Distinct().OrderBy(p => p);

            return PartialView(categories);
        }

        public PartialViewResult SideBarVendors(string vendor)
        {
            ViewBag.SelectedVendor = vendor;

            var vendors = _unitOfWork.ProductRepository.GetAllProducts()
                .Select(p => p.Vendor).Distinct().OrderBy(p => p);

            return PartialView(vendors);
        }

        public ActionResult Cart(Cart cart, string returnUrl)
        {
            var model = new CartViewModel
            {
                CartItems = _unitOfWork.CartRepository.GetCartItemsFor(cart.Id),

                CartFormViewModel = new CartFormViewModel
                {
                    ReturnUrl = returnUrl
                }
            };

            return View(model);
        }


        [HttpPost]
        public RedirectToRouteResult AddToCart(Cart cart, ProductPageViewModel model)
        {
            if (!ModelState.IsValid)
            {
                TempData["cart"] = "An Error Occured";
                return RedirectToAction("Index", "Shop");
            }


            var product = _unitOfWork.ProductRepository.GetProduct(model.CartFormViewModel.ProductId);

            if (product != null)
            {
                if (product.Quantity > model.CartFormViewModel.Quantity)
                {
                    _unitOfWork.CartRepository.AddItem(product, model.CartFormViewModel.Quantity, cart);
                    _unitOfWork.CartRepository.Save();
                    TempData["cart"] = $"{product.Name} added to Cart";
                    return RedirectToAction("Cart", new {model.CartFormViewModel.ReturnUrl});
                }

                TempData["cartError"] = "Number of Items to add are More than Items In Stock, Choose a lower number";
            }

            return RedirectToAction("Index", "Shop");
        }


        [HttpPost]
        public RedirectToRouteResult UpdateCart(Cart cart, CartFormViewModel model)
        {
            var product = _unitOfWork.ProductRepository.GetProduct(model.ProductId);
            if (product != null && product.Quantity > model.Quantity)
            {
                _unitOfWork.CartRepository.UpdateCart(model.ProductId, model.Quantity, cart);
                _unitOfWork.CartRepository.Save();
            }
            else
            {
                TempData["errMsg"] = "Number of Items to add are More than Items In Stock, Choose a lower number";
            }

            return RedirectToAction("Cart", "Shop", new {model.ReturnUrl});
        }

        public PartialViewResult CartCount(Cart cart)
        {
            return PartialView(_unitOfWork.CartRepository.GetCartItemsFor(cart.Id));
        }


        [Authorize]
        public ActionResult CheckOut(Cart cart)
        {
            var model = new CheckOutViewModel
            {
                CartItems = _unitOfWork.CartRepository.GetCartItemsFor(cart.Id),
                OrderViewModel = new OrderViewModel
                {
                    UserId = User.Identity.GetUserId()
                }
            };


            if (!model.CartItems.Any()) return RedirectToAction("Index", "Shop");
            var cartItems = model.CartItems
                .Where(
                    p => p.Product.Quantity > 0 && p.Product.ExpireDate == null
                         || p.Product.Quantity > 0 && p.Product.ExpireDate != null &&
                         p.Product.ExpireDate.Value > DateTime.Now)
                .ToList();
            model.CartItems = cartItems;
            var total = cartItems.Sum(c => c.Quantity * c.Product.Price);
            if (total > 0) return View(model);

            return RedirectToAction("Index", "Shop");
        }

        public RedirectToRouteResult ClearCart(Cart cart)
        {
            _unitOfWork.CartRepository.Clear(cart);
            _unitOfWork.CartRepository.Save();
            return RedirectToAction("Cart", "Shop");
        }


        [Authorize]
        public ActionResult Order(int id)
        {
            var model = _unitOfWork.OrderRepository.GetOrderFor(id, User.Identity.GetUserId());

            return View(model);
        }


        [Authorize]
        public ActionResult OrderComplete()
        {
            return View();
        }


        [Authorize]
        [HttpPost]
        public ActionResult ProcessOrder(Cart cart, CheckOutViewModel model)
        {
            if (!ModelState.IsValid) return View("CheckOut", model);

            var processOrder = _unitOfWork.OrderRepository.CreateOrder(model.OrderViewModel, cart);

            if (processOrder)
            {
                _unitOfWork.CartRepository.Clear(cart);
                _unitOfWork.CartRepository.Save();
                return RedirectToAction("OrderComplete", "Shop");
            }

            return View("CheckOut", model);
        }


        [HttpPost]
        public RedirectToRouteResult RemoveCartItem(Cart cart, CartFormViewModel model)
        {
            var product = _unitOfWork.ProductRepository.GetProduct(model.ProductId);

            if (product != null)
            {
                _unitOfWork.CartRepository.RemoveItem(product, cart);
                _unitOfWork.CartRepository.Save();
            }

            return RedirectToAction("Cart", new { model.ReturnUrl });
        }


        public PartialViewResult SideBarCart(Cart cart)
        {
            var model = new CartViewModel
            {
                CartItems = _unitOfWork.CartRepository.GetCartItemsFor(cart.Id)
            };
            return PartialView(model);
        }


        
        [Authorize]
        public PartialViewResult UserOrders()
        {
            var model = _unitOfWork.OrderRepository.GetAllOrdersPerCustomer(User.Identity.GetUserId());
            return PartialView(model);
        }

        
    }
}