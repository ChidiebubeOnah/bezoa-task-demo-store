﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Dtos
{
    public class UpdateUserDto
    {

        public UpdateUserDto()
        {
            UpdatedAt = DateTime.Now;
        }
        [Required]
        public string Name { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public int GenderId { get; set; }

        [Required]
        public string Role { get; set; }

        [Required]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }
        public DateTime UpdatedAt { get;  }
    }
}