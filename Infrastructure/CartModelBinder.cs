﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Ninject;
using Ninject.Activation;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Repositories;
using Store_Management_App.Services;
using WebGrease.Css.Extensions;

namespace Store_Management_App.Infrastructure
{
    public class CartModelBinder: IModelBinder
    {
        private readonly ICartRepository _cartRepo;
        private readonly IKernel _kernel = new StandardKernel();
        
       
        public CartModelBinder( )
        {
           _kernel.Bind<ICartRepository>().To<CartRepository>(); 
           _cartRepo = _kernel.Get<ICartRepository>();
            
        }


        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var cart = CartInitializer.Init(_cartRepo);
            var guestCart = _cartRepo.GetCart(GuestSession.SessionId());
            if (controllerContext.HttpContext.Request.IsAuthenticated && guestCart.CartItems.Count > 0)
            {
                var guestCartItems = _cartRepo.GetCartItemsFor(guestCart.Id);
                guestCartItems.ForEach(i => _cartRepo.AddItem(i.Product, i.Quantity, cart));
                _cartRepo.Clear(guestCart);
                cart.UpdatedAt = DateTime.Now;
                _cartRepo.Save();
            }

            return cart;
        }
    }
}