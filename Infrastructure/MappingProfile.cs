﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using Store_Management_App.Dtos;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<UpdateUserDto, ApplicationUser>();
            Mapper.CreateMap<EditUserViewModel, ApplicationUser>();
            Mapper.CreateMap<EditCustomerViewModel, ApplicationUser>();

            Mapper.CreateMap<ProductViewModel, Product>();
            Mapper.CreateMap<Product, ProductViewModel>();

            Mapper.CreateMap<OrderViewModel, Order>();

        }
    }
}