﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using Ninject.Web.Common;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Repositories;

public class NinjectResolver : IDependencyResolver
{
    private IKernel _kernel;
    public NinjectResolver(IKernel kernel)
    {
        _kernel = kernel;
        AddBindings();
    }

    private void AddBindings()
    {
        _kernel.Bind<IAppUserRepository>().To<AppUserRepository>();
        _kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
        _kernel.Bind<IProductRepository>().To<ProductRepository>();
        _kernel.Bind<IOrderRepository>().To<OrderRepository>();
        _kernel.Bind<ICartRepository>().To<CartRepository>();
        _kernel.Bind<IActivityRepository>().To<ActivityRepository>();
        _kernel.Bind<IVendorRepository>().To<VendorRepository>();
        _kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
        _kernel.Bind<ApplicationDbContext>().ToSelf();
    }

    public object GetService(Type serviceType) => _kernel.TryGet(serviceType);

    public IEnumerable<object> GetServices(Type serviceType) => _kernel.GetAll(serviceType);
}