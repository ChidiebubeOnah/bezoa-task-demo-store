﻿using System.Collections.Generic;
using Store_Management_App.Models;

namespace Store_Management_App.Interfaces
{
    public interface IActivityRepository
    {
        void Create(string userId, int orderId, string action);
        IEnumerable<Activity> GetActivities();
        void Save();
    }
}