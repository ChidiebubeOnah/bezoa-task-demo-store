﻿using System.Collections.Generic;
using Microsoft.AspNet.Identity.EntityFramework;
using Store_Management_App.Dtos;
using Store_Management_App.Infrastructure;
using Store_Management_App.Models;
using Store_Management_App.Models.DataLayers;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Interfaces
{
    public interface IAppUserRepository
    {
        IEnumerable<UserDataView> GetAllUsers(string role);
        UserDataView GetUser(string id);
        bool UpdateUser(string id, EditUserViewModel model);
        bool UpdateCustomer(string id, EditCustomerViewModel model);
        ApplicationUser GetUserById(string id);
        IEnumerable<Gender> GetGenders();
        IEnumerable<IdentityRole> GetRoles();
        void CreateUser(RegisterViewModel user);
        void UpdateUser(string id ,  UpdateUserDto dto);
        void DeleteUser(string id);
    }
}