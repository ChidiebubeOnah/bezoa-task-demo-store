﻿using System.Collections.Generic;
using Store_Management_App.Models;

namespace Store_Management_App.Interfaces
{
    public interface ICartRepository
    {
        void Create(string userId);
        Cart GetCart(string userId);
        void AddItem(Product product, int quantity, Cart cart);
        IEnumerable<CartItem> GetCartItemsFor(int cartId);
        void UpdateCart(int id, int quantity, Cart cart);
        void RemoveItem(Product product, Cart cart);
        double GetTotalValue(int cardId);
        void Clear(Cart cart);

        void Save();
    }
}