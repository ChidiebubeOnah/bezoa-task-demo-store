﻿using System.Collections.Generic;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Interfaces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAllCategories();
        Category GetCategory(int id);
        bool UpdateCategory(int id, CategoryViewModel model);
        void CreateCategory(Category model);
        void DeleteCategory(int id);
    }
}