﻿using System.Collections.Generic;
using Store_Management_App.Dtos;
using Store_Management_App.Infrastructure;
using Store_Management_App.Models;
using Store_Management_App.Models.DataLayers;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Interfaces
{
    public interface IOrderRepository
    {
        IEnumerable<OrderDataView> GetAllOrders();
        IEnumerable<Order> GetAllOrdersPerCustomer(string userId);

        double TotalSales();
        bool CreateOrder(OrderViewModel model, Cart cart);
        IEnumerable<OrderItem> GetOrderItemsFor(int orderId, string userId);
        Order GetOrderFor(int id, string userId);
        void Save();
        Order GetOrder(int id);
        void UpdateOrder(int id, UpdateOrderDto dto);
        bool DeleteOrder(int id);
    }
}