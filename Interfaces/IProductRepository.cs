﻿using System.Collections.Generic;
using System.Web;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<ProductDataView> GetAllProducts(string category = null, string vendor = null);
        IEnumerable<ProductDataView> GetAllProductsPerSearch(string category = null);

        

        IEnumerable<ProductDataView> ProductsPerPage(int pageSize, string category, string vendor, string query, int page = 1);
        ProductDataView GetProductDataView(int id);
        Product GetProduct(int id);
        string SaveProduct(ProductViewModel model, HttpPostedFileBase photo = null);
        bool DeleteProduct(int id);
    }
}