using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Store_Management_App.Models;
using Store_Management_App.Services;

namespace Store_Management_App.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Store_Management_App.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Store_Management_App.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var genders = new List<Gender>()
            {
                new Gender()
                {

                    Name = "Male",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                new Gender()
                {
                    Name = "Female",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                },
                new Gender()
                {
                    Name = "Prefer Not to Specify",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            };

             genders.ForEach( g => context.Genders.AddOrUpdate(g));

            /*Create Some Roles*/

            var role = new CreateRoleService(context);

            role.Create("Admin");
            role.Create("Customer");

            ApplicationUser superAdmin = new ApplicationUser()
            {
                Name = "Admin",
                Email = "admin@domain.com",
                EmailConfirmed = true,
                GenderId = 1,
                UserName = "SuperAdmin",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };
            IdentityResult result = manager.Create(superAdmin, "P@55 w@rd");
            if (result.Succeeded)
            {
                manager.AddToRole(superAdmin.Id, "Admin");
            }
        }
    }
}
