﻿using System;

namespace Store_Management_App.Models
{
    public class Activity
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public ApplicationUser User  { get; set; }
        public string ActionPerformed { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }

        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}

