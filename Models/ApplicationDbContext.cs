﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Drawing.Printing;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Store_Management_App.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {


        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Gender> Genders { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Vendor> Vendors { get; set; }

        public DbSet<File> Files { get; set; }

        public DbSet<Cart> Carts { get; set; }
        public DbSet<CartItem> CartItems { get; set; }
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        // protected override void OnModelCreating(DbModelBuilder modelBuilder)
        // {
        //     modelBuilder.Entity<Product>()
        //         .HasRequired(p => p.File)
        //         .WithRequiredPrincipal();
        //
        //     modelBuilder.Entity<Order>()
        //         .HasRequired(o => o.OrderItems)
        //         .WithMany();
        //        
        //
        //
        //     base.OnModelCreating(modelBuilder);
        // }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}