﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Store_Management_App.Models
{
    public class Cart
    {
        public Cart()
        {
            CartItems = new List<CartItem>();
        }

        public int Id { get; set; }

        public string UserId { get; set; }

        public ICollection<CartItem> CartItems { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }



        public double GetTotalValue()
        {
            return CartItems.Sum(c => c.Product.Price * c.Quantity);
        }



    }
}