﻿using System;
using System.Collections.Generic;

namespace Store_Management_App.Models.DataLayers
{
    public class UserDataView
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
       
    }
}