﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebGrease.Activities;

namespace Store_Management_App.Models
{
    public class File
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
        public FileType Type { get; set; }
     


        public DateTime  CreatedAt { get; set; } = DateTime.Now;
    }

    public enum FileType
    {
        Photo = 1
    }
}