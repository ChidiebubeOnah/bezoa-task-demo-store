﻿using System;
using Store_Management_App.Models;

public class OrderItem
{
    public int Id { get; set; }
    public int ProductId { get; set; }
    public Product Product { get; set; }
    public int OrderId { get; set; }
    public Order Order { get; set; }
    public double Price { get; set; }   
    public int Quantity { get; set; }
    public DateTime UpdatedAt { get; set; } = DateTime.Now;
    public DateTime CreatedAt { get; set; } = DateTime.Now;

    public double GetCost()
    {
        return Price * Quantity;
    }
}