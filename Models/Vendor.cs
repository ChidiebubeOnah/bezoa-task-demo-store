﻿using System;

namespace Store_Management_App.Models
{
    public class Vendor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}