﻿using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Models.ViewModels
{
    public class CartFormViewModel
    {
        public int  ProductId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Quantity Should be Atleast One")]
        public int Quantity { get; set; }

        public string ReturnUrl { get; set; }
    }
}