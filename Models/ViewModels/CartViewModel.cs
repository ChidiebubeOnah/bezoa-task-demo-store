﻿using System.Collections.Generic;

namespace Store_Management_App.Models.ViewModels
{
    public class CartViewModel
    {
        public CartFormViewModel CartFormViewModel { get; set; }
        
        public IEnumerable<CartItem> CartItems { get; set; }
        public double Total { get; set; }
    }
}
