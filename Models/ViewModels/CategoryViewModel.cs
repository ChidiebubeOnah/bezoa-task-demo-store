﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Models.ViewModels
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter a Category")]
        [Display(Name = "Category")]
        public string Name { get; set; }

        public IEnumerable<Category> Categories { get; set; }
        public DateTime UpdatedAt { get;  } = DateTime.Now;
        public DateTime CreatedAt { get; } = DateTime.Now;
    }
}