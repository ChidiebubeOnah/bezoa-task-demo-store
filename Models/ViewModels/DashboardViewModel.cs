﻿using System.Collections.Generic;

namespace Store_Management_App.Models.ViewModels
{
    public class DashboardViewModel
    {
        public int Customers { get; set; }
        public int Products { get; set; }
        public int Orders { get; set; }
        public double TotalSales { get; set; }

        public IEnumerable<Activity> Activities { get; set; }

    }
}