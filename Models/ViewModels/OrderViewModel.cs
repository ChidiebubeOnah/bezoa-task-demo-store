﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Models.ViewModels
{
    public class OrderViewModel
    {
    
        [Required]
        public string UserId { get; set; }
    
        public OrderStatus Status { get; } = OrderStatus.Pending;

        [Required(ErrorMessage = "Full Name Can't Be Blank")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Company Name Can't Be Blank")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Street Address Can't Be Blank")]
        public string Address1 { get; set; }
        public string Address2 { get; set; }

        [Required(ErrorMessage = "Town/City  Can't Be Blank")]
        public string Town { get; set; }

        [Required(ErrorMessage = "Postal Code  Can't Be Blank")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Country/State  Can't Be Blank")]
        public string State { get; set; }

        [Required(ErrorMessage = "Phone Number  Can't Be Blank")]
        [Phone]
        public string Phone { get; set; }


        [Required(ErrorMessage = "Email Address  Can't Be Blank")]
        [EmailAddress]
        public string Email { get; set; }
        public DateTime UpdatedAt { get;  } = DateTime.Now;
        public DateTime CreatedAt { get;  } = DateTime.Now;
    }
}