﻿namespace Store_Management_App.Models.ViewModels
{
    public class ProdDetailViewModel
    {
        public CartFormViewModel CartFormViewModel  { get; set; }
        public ProductDataView Product { get; set; }
    }
}