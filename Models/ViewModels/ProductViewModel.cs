﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Models.ViewModels
{
    public class ProductViewModel
    {
        
        public int Id { get; set; }

        [Required(ErrorMessage = "Sku Can't Be blank!")]
        public string Sku { get; set; }

        [Required(ErrorMessage = "Name Can't Be blank!")] 
        public string Name { get; set; }

        [Required(ErrorMessage = "Category Select a Category!")] 
        [DisplayName("Category")]
        public int CategoryId { get; set; }
        public IEnumerable<Category> Categories { get; set; }

        [Required(ErrorMessage = "Category Select a Vendor!")]
        [DisplayName("Vendor")]
        public int VendorId { get; set; }
        public IEnumerable<Vendor> Vendors { get; set; }

        [Required(ErrorMessage = "Give A Description!")] 
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Required(ErrorMessage = "Enter Price!")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Invalid Input")]
        public double Price { get; set; }


        [Required(ErrorMessage = "Enter Quantity!")]
        [Range(10, int.MaxValue, ErrorMessage = "Number Should be at least 10")]
        public int Quantity { get; set; }
            

        [DisplayName("Best Before")]
        public DateTime? ExpireDate { get; set; }


        public File File { get; set; }

        public DateTime UpdatedAt { get;  } = DateTime.Now;
        public DateTime CreatedAt { get;  } = DateTime.Now;

    }
}