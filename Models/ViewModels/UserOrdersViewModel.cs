﻿using System.Collections.Generic;

namespace Store_Management_App.Models.ViewModels
{
    public class UserOrdersViewModel
    {
        public Order Order { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }
    }
}