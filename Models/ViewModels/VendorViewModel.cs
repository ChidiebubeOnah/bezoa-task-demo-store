﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Store_Management_App.Models.ViewModels
{
    public class VendorViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter a Vendor")]
        [Display(Name = "Vendor")]
        public string Name { get; set; }

        public IEnumerable<Vendor> Vendors { get; set; }
        public DateTime UpdatedAt { get; } = DateTime.Now;
        public DateTime CreatedAt { get; } = DateTime.Now;
    }
}