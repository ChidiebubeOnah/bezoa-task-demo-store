﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;

namespace Store_Management_App.Repositories
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly ApplicationDbContext _context;

        public ActivityRepository(ApplicationDbContext context)
        {
            _context = context;
        }


        public void Create(string userId, int orderId, string action)
        {
            _context.Activities.Add(
                new Activity
                {
                    UserId = userId,
                    OrderId = orderId,
                    ActionPerformed = action,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            );

        }

       
        public IEnumerable<Activity> GetActivities()
        {
           return _context.Activities.AsQueryable().Include(p => p.User).OrderByDescending(a => a.CreatedAt).ToList();
        }
        public void Save()
        {
            _context.SaveChanges();
        }

    }
}