﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Store_Management_App.Dtos;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.DataLayers;
using Store_Management_App.Models.ViewModels;
using WebGrease.Css.Extensions;

namespace Store_Management_App.Repositories
{
    public class AppUserRepository : IAppUserRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;

        private ApplicationUserManager _userManager =
            HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

        public AppUserRepository(ApplicationDbContext context )
        {
            _context = context;
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
        }
        public IEnumerable<UserDataView> GetAllUsers(string role)
        {
         var userIdInRole   = _roleManager.FindByName(role).Users.Select(u => u.UserId).ToList();
         return _context.Users.Include(u => u.Gender)
             .Where(u => userIdInRole.Contains(u.Id))
             .Select( user => new UserDataView()
               {   Id = user.Id,
                   Name =  user.Name,
                   UserName =  user.UserName,
                   Email =  user.Email,
                   Gender = user.Gender.Name,
                   Roles = _context.Roles
                       .Where(c => user.Roles
                           .Select(a => a.RoleId)
                           .ToList().Contains(c.Id))
                       .Select( r => r.Name).ToList(),
                   PhoneNumber = user.PhoneNumber,
                   UpdatedAt = user.UpdatedAt,
                   CreatedAt = user.CreatedAt
               } )
               .ToList();
        }


        public UserDataView GetUser(string id)
        {
            return _context.Users
                .Include(u => u.Gender)
                .Select(user => new UserDataView()
                {
                    Id = user.Id,
                    Name = user.Name,
                    UserName = user.UserName,
                    Email = user.Email,
                    Gender = user.Gender.Name,
                    Roles = _context.Roles
                        .Where(c => user.Roles
                            .Select(a => a.RoleId)
                            .ToList().Contains(c.Id))
                        .Select(r => r.Name).ToList(),
                    PhoneNumber = user.PhoneNumber,
                    UpdatedAt = user.UpdatedAt,
                    CreatedAt = user.CreatedAt
                })
                .SingleOrDefault(u => u.Id == id);

        }

        public bool UpdateCustomer(string id, EditCustomerViewModel model)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            if (user != null)
            {

                var userNameExist = _context.Users.AsQueryable().Any(c => c.UserName.ToLower() == model.UserName.ToLower());

                if (userNameExist && user.UserName != model.UserName)
                {
                    return false;
                }

                Mapper.Map(model, user);

                user.UpdatedAt = DateTime.Now;

               
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public ApplicationUser GetUserById(string id)
        {
            return _context.Users
                .Include(u => u.Gender)
                .SingleOrDefault(u => u.Id == id);

        }

        public IEnumerable<Gender> GetGenders()
        {
            return _context.Genders.ToList();
        }

        public IEnumerable<IdentityRole> GetRoles()
        {
            return  _context.Roles.ToList();
        }

        public void CreateUser(RegisterViewModel user)
        {
            
        }

        public void UpdateUser(string id, UpdateUserDto dto)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            if (user != null)
            {
                Mapper.Map(dto, user);

                var currentRoleId = user.Roles.Select(r => r.RoleId).SingleOrDefault();
                var currentRoleName = GetRoles().SingleOrDefault(r => r.Id == currentRoleId)?.Name;
                if (currentRoleName != dto.Role)
                {

                    if (_roleManager.RoleExists(dto.Role))
                    {
                        if (currentRoleName != null)
                        {
                            _userManager.RemoveFromRole(user.Id, currentRoleName);
                        }

                        _userManager.AddToRole(user.Id, dto.Role);
                    }
                }
                user.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
            }

        }

        public  bool UpdateUser(string id, EditUserViewModel model)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            if (user != null)
            {
                var userNameExist = _context.Users.AsQueryable().Any(c => c.UserName.ToLower() == model.UserName.ToLower());

                if (userNameExist && user.Name != model.UserName)
                {
                    return false;
                }
                Mapper.Map(model, user);

                var currentRoleId = user.Roles.Select(r => r.RoleId).SingleOrDefault();
                var currentRoleName = GetRoles().SingleOrDefault(r => r.Id == currentRoleId)?.Name;
                if (currentRoleName != model.Role)
                {

                    if (_roleManager.RoleExists(model.Role))
                    {
                        if (currentRoleName != null)
                        {
                            _userManager.RemoveFromRole(user.Id, currentRoleName);
                        }

                        _userManager.AddToRole(user.Id, model.Role);
                    }
                }
                user.UpdatedAt = DateTime.Now;
              

                _context.SaveChanges();
                return true;
            }

            return false;

        }

        public void DeleteUser(string id)
        {
            var user = _context.Users.SingleOrDefault(u => u.Id == id);
            _context.Users.Remove(user);
            _context.SaveChanges();
        }
    }
}