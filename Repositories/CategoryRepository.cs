﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Category> GetAllCategories()
        {
            return  _context.Categories.ToList();
        }

        public Category GetCategory(int id)
        {
            return  _context.Categories.SingleOrDefault(c => c.Id == id);
        }

        public bool UpdateCategory(int id, CategoryViewModel model)
        {
            var category = _context.Categories.SingleOrDefault(c => c.Id == id);
            if (category != null)
            {
                category.Name = model.Name;
                category.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public void CreateCategory(Category model)
        {
            _context.Categories.Add(model);
            _context.SaveChanges();
        }

        public void DeleteCategory(int id)
        {
            var category = _context.Categories.SingleOrDefault(c => c.Id == id);


            _context.Categories.Remove(category);
            _context.SaveChanges();
        }
    }
}