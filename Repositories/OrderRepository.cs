﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Store_Management_App.Dtos;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.DataLayers;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IActivityRepository _activityRepo;

        public OrderRepository(ApplicationDbContext context, IActivityRepository activityRepo)
        {
            _context = context;
            _activityRepo = activityRepo;
        }

        public IEnumerable<OrderDataView> GetAllOrders()
        {
            return _context.Orders.Include( p => p.User).Include(o => o.OrderItems)
                .Select( c =>  new OrderDataView
                {
                    Id = c.Id,
                    User = new ApplicationUserDateView
                    {
                        Name = c.User.Name,
                        Gender = c.User.Gender.Name,
                        Phone = c.User.PhoneNumber,
                        Email = c.User.Email
                    },
               
                    Total = c.OrderItems.Sum( _ => (_.Price * _.Quantity)),
                    Status = c.Status,
                    CreatedAt = c.CreatedAt,
                    UpdatedAt = c.UpdatedAt
                })
                .ToList();
        }

        public IEnumerable<Order> GetAllOrdersPerCustomer(string userId)
        {
            return _context.Orders.Include(o => o.OrderItems).Where(o => o.UserId == userId).ToList();
        }

        public double TotalSales()
        {
            var orders = _context.Orders.Include(o => o.OrderItems).Where(c => c.Status == OrderStatus.Completed);
            if (orders.Any())
            {
                return orders.Sum(s => s.OrderItems.Sum(_ => _.Price * _.Quantity));

            }

            return 0;
        }
    


        public bool CreateOrder(OrderViewModel model, Cart cart)
        {
            Order order = Mapper.Map<OrderViewModel, Order>(model);

            _context.Orders.Add(order);

            var orderItems = new List<OrderItem>();
            var cartItem = cart.CartItems.AsQueryable()
                .Include(c => c.Product)
                .Where(
                    p =>( p.Product.Quantity > 0 && p.Product.ExpireDate == null)
                        || (p.Product.Quantity > 0
                            && (p.Product.ExpireDate != null
                                && p.Product.ExpireDate.Value > DateTime.Now)
                        )).ToList();

            foreach (var item in cartItem)
            {
                orderItems.Add(
                    new OrderItem
                    {
                        OrderId = order.Id,
                        Price = item.Product.Price,
                        ProductId = item.Product.Id,
                        Quantity = item.Quantity
                    });

                var product = _context.Products.SingleOrDefault(p => p.Id == item.Product.Id);
                if (product != null) product.Quantity -= item.Quantity;
            }
            _context.OrderItems.AddRange(orderItems);
            CreateActivity(order.UserId, order.Id, "Placed");

            _context.SaveChanges();

            return true;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public IEnumerable<OrderItem> GetOrderItemsFor(int orderId, string userId)
        {
            return GetOrderFor(orderId, userId).OrderItems.AsQueryable().Include(c => c.Product).ToList();
        }
        public Order GetOrderFor(int id, string userId)
        {

            return _context.Orders.Include(c => c.OrderItems.Select(p => p.Product))
                .SingleOrDefault(_ => _.Id == id && _.UserId == userId);
        }

        public Order GetOrder(int id)
        {
            return _context.Orders.Include(c => c.OrderItems.Select(p => p.Product)).Include(p => p.User).SingleOrDefault(p => p.Id == id);
        }

        private void CreateActivity(string userId, int orderId, string action)
        {
            _context.Activities.Add(
                new Activity
                {
                    UserId = userId,
                    OrderId = orderId,
                    ActionPerformed = action,
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now
                }
            );
        }
        public void UpdateOrder(int id, UpdateOrderDto dto)
        {
            var order = GetOrder(id);

            var userId = HttpContext.Current.User.Identity.GetUserId();

            switch (dto.Action.ToLower())
            {
                case "complete":
                    if (order.Status != OrderStatus.Completed)
                    {
                        order.Status = OrderStatus.Completed;
                        order.UpdatedAt = DateTime.Now;
                        CreateActivity(userId, id, "Completed");
                    }

                    break;
                case "cancel":

                    if (order.Status != OrderStatus.Cancelled)
                    {
                        order.Status = OrderStatus.Cancelled;
                        order.UpdatedAt = DateTime.Now;
                        CreateActivity(userId, id, "Cancelled");
                    }
              
                    break;
                default:
                    break;
            }
        }



        public bool DeleteOrder(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}