﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;

namespace Store_Management_App.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;


        public IOrderRepository OrderRepository { get; }
        public IVendorRepository VendorRepository { get; }
        public IActivityRepository ActivityRepository { get; }
        public IAppUserRepository AppUserRepository { get; }
        public ICartRepository CartRepository { get; }
        public ICategoryRepository CategoryRepository { get; }
        public IProductRepository ProductRepository { get; }

        public UnitOfWork(

            ApplicationDbContext context,
            ICartRepository cartRepository, 
            IOrderRepository orderRepository, 
            IVendorRepository vendorRepository,
            IActivityRepository activityRepository,
            IAppUserRepository appUserRepository,
            ICategoryRepository categoryRepository,
            IProductRepository productRepository

            )
        {
            _context = context;
            CartRepository = cartRepository;
            OrderRepository = orderRepository;
            VendorRepository = vendorRepository;
            ActivityRepository = activityRepository;
            AppUserRepository = appUserRepository;
            CategoryRepository = categoryRepository;
            ProductRepository = productRepository;
        }


    }
}