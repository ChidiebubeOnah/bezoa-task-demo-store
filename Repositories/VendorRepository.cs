﻿using System;
using System.Collections.Generic;
using System.Linq;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Repositories
{
    public class VendorRepository : IVendorRepository
    {
        private readonly ApplicationDbContext _context;

        public VendorRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public IEnumerable<Vendor> GetAllVendors()
        {
            return _context.Vendors.ToList();
        }

        public Vendor GetVendor(int id)
        {
            return _context.Vendors.SingleOrDefault(c => c.Id == id);
        }

        public bool UpdateVendor(int id, VendorViewModel model)
        {
            var vendor = _context.Vendors.SingleOrDefault(c => c.Id == id);
            if (vendor != null)
            {
                vendor.Name = model.Name;
                vendor.UpdatedAt = DateTime.Now;
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public void CreateVendor(Vendor model)
        {
            _context.Vendors.Add(model);
            _context.SaveChanges();
        }

        public void DeleteVendor(int id)
        {
            var  vendor = _context.Vendors.SingleOrDefault(c => c.Id == id);

            if (vendor != null)
            {
                _context.Vendors.Remove(vendor);
                _context.SaveChanges();
            }
               
        }
    }
}