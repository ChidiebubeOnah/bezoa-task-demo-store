﻿using System.Web;
using Microsoft.AspNet.Identity;
using Store_Management_App.Infrastructure;
using Store_Management_App.Interfaces;
using Store_Management_App.Models;

namespace Store_Management_App.Services
{
    public class CartInitializer
    {

        public static Cart Init(ICartRepository cartRepo)
        {
            
            var userId = HttpContext.Current.User.Identity.GetUserId() ?? GuestSession.SessionId();
            Cart cart = cartRepo.GetCart(userId);
            if (cart != null)
            {
                return cart;

            }
            cartRepo.Create(userId);
            cartRepo.Save();
            return cartRepo.GetCart(userId);

        }

      
    }
}