﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Store_Management_App.Models;

namespace Store_Management_App.Services
{
    public class CreateRoleService
    {
        public CreateRoleService( ApplicationDbContext context)
        {
            _userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
        }
        private  readonly UserManager<ApplicationUser> _userManager;
        private  readonly RoleManager<IdentityRole> _roleManager; 
        
        public  void Create(string name)
        {
            if (! _roleManager.RoleExists(name))
            {
                IdentityRole role = new IdentityRole();
                role.Name = name;
                _roleManager.Create(role);
            }
        }
    }
}