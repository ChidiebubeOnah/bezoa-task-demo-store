﻿using System.Web;
using Store_Management_App.Models;
using Store_Management_App.Models.ViewModels;

namespace Store_Management_App.Services
{
    public class UpsertFile
    {
        public static void Save(ProductViewModel model, HttpPostedFileBase photo = null)
        {
            if (model.File != null)
            {
                model.File.Name = System.IO.Path.GetFileName(photo.FileName);
                model.File.Type = FileType.Photo;
                model.File.ContentType = photo.ContentType;
                using (var reader = new System.IO.BinaryReader(photo.InputStream))
                {
                    model.File.Content = reader.ReadBytes(photo.ContentLength);
                }
            }
            else
            {
                model.File = new File()
                {
                    Name = System.IO.Path.GetFileName(photo.FileName),
                    Type = FileType.Photo,
                    ContentType = photo.ContentType
                };
                using (var reader = new System.IO.BinaryReader(photo.InputStream))
                {
                    model.File.Content = reader.ReadBytes(photo.ContentLength);
                }

            }
        }
    }
}