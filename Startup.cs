﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Store_Management_App.Startup))]
namespace Store_Management_App
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
